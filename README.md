# University of Arizona Java 8 and Tomcat 8 Docker Image

---

This repository is for the Kuali team's Tomcat 8 image based on their CentOS 6 + Java 8 Docker image.

### Description
This project defines an image that will be the base image for the Kuali team's KFS and Rice Docker images.

### Requirements
This is based on a **java8** tagged image from the _397167497055.dkr.ecr.us-west-2.amazonaws.com/kuali/tomcat7_ AWS ECR repository. (Until we update our version of Java, this is unchanged from what we did for the java8tomcat7 Docker image.)

The included version of Apache Tomcat was downloaded from https://archive.apache.org/dist/tomcat/tomcat-8/v8.5.30/bin/ to reduce external dependencies at build time.

### Building
#### Locally
If you are testing changes and building the image locally, follow these steps:

##### Without AWS Credentials
1. Make sure you have a `kuali/tomcat7:java8-ua-release-$BASE_IMAGE_TAG_DATE` image in your Docker repository. (This may require a local build of the *java8* base image).
2. Temporarily change the Dockerfile to define the base image as `FROM kuali/tomcat7:java8-ua-release-$BASE_IMAGE_TAG_DATE`.
3. Run this command to build a *java8tomcat8* Docker image, replacing $BASE_IMAGE_TAG_DATE with the date referenced in step 1: `docker build --build-arg BASE_IMAGE_TAG_DATE=$BASE_IMAGE_TAG_DATE -t kuali/tomcat8 .`

##### With AWS Credentials
1. Follow instructions on Confluence to get temporary AWS credentials: [Get Temporary AWS credentials for an account](https://confluence.arizona.edu/display/KAST/Get+Temporary+AWS+credentials+for+an+account)
2. Build the image with the same command used in Jenkins. For example: `docker build --build-arg DOCKER_REGISTRY=397167497055.dkr.ecr.us-west-2.amazonaws.com --build-arg BASE_IMAGE_TAG_DATE=2018-09-24 -t kuali/tomcat8 .`

This requires a local install of the AWS CLI: https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html.

_This method is untested at this current time._

#### Jenkins
The build command we use is: `docker build --build-arg DOCKER_REGISTRY=${DOCKER_REGISTRY} --build-arg BASE_IMAGE_TAG_DATE=${BASE_IMAGE_TAG_DATE} -t ${DOCKER_REPOSITORY_NAME}:${TAG} .`

* `$DOCKER_REGISTRY` is the location of the Docker image repository in AWS. The value will be a variable in our Jenkins job and defined as `397167497055.dkr.ecr.us-west-2.amazonaws.com`.
* `$BASE_IMAGE_TAG_DATE` corresponds to the creation date in a tag of the *java8* Docker image.
* `$DOCKER_REPOSITORY_NAME` is the name of the AWS ECR repository, which is _kuali/tomcat8_.
* `$TAG` is the image tag we construct, which is _java8tomcat8-ua-release-_ plus the current date in YYYY-MM-DD format.

We then tag and push the image to AWS with these commands: 
```
docker tag ${DOCKER_REPOSITORY_NAME}:${TAG} ${DOCKER_REPOSITORY_URI}:${TAG}
docker push ${DOCKER_REPOSITORY_URI}:${TAG}
```

Example of a resulting tag: _397167497055.dkr.ecr.us-west-2.amazonaws.com/kuali/tomcat8:java8tomcat8-ua-release-2019-05-01_.

Jenkins link: https://kfs-jenkins.ua-uits-kuali-nonprod.arizona.edu/job/Development/view/Docker%20Baseline/

### Maintenance Notes
If a newer *java8* image has been created, a newer version of this *java8tomcat8* image will need to be created. The date is controlled by the value for the `$BASE_IMAGE_TAG_DATE`.

The Dockerfiles for KFS and Rice will need to be updated to use this new version of the java8tomcat8 image based on the creation date.