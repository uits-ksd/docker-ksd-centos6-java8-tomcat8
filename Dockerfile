# Tomcat 8
ARG  DOCKER_REGISTRY
ARG  BASE_IMAGE_TAG_DATE
FROM $DOCKER_REGISTRY/kuali/tomcat7:java8-ua-release-$BASE_IMAGE_TAG_DATE
MAINTAINER U of A Kuali DevOps <katt-support@list.arizona.edu>

# Set environment variables
ENV CATALINA_HOME /opt/tomcat

# Install updates
RUN yum update -y && \
    rm -rf /var/lib/apt/lists/*

# Manually install Tomcat from included download
COPY bin .
RUN tar -xvf apache-tomcat-8.5.30.tar.gz && \
    rm apache-tomcat-8.5.30.tar.gz && \
    mv apache-tomcat* ${CATALINA_HOME}

RUN chmod +x $CATALINA_HOME/bin/*sh

# Create links that KFS image expects (TODO should be vice-versa; update KFS image to be CentOS'y)
RUN ln -s ${CATALINA_HOME} /var/lib/tomcat
  
EXPOSE 8080
